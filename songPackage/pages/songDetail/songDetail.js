// pages/songDetail/songDetail.js
import request from "../../../utils/request";
import PubSub from 'pubsub-js';
import moment from 'moment'
//获取全局实例
const appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isPlay: false, //音乐是否播放
    songId: '',
    song: {},
    songLocation: '',
    currentTime: '00:00',
    durationTime: '00:00',
    currentWidth: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let songId = JSON.parse(options.songId)
    this.setData({
      songId
    });

    if (appInstance.globalData.isMusicPlay && appInstance.globalData.musicId === songId){
      this.changePlayState(true);
    }

    this.getSongData();



    //创建音乐播放的实例
    this.backgroundAudioManager = wx.getBackgroundAudioManager()
    this.backgroundAudioManager.onPlay(() => {
      this.changePlayState(true);

      //修改全局变量
      appInstance.globalData.musicId = songId;
    });
    this.backgroundAudioManager.onPause(() => {
      this.changePlayState(false);
    });
    //音乐停止的状态
    this.backgroundAudioManager.onStop(() => {
      this.changePlayState(false);
    });
    //监听音乐实时播放进度
    this.backgroundAudioManager.onTimeUpdate(() => {
      let currentTime = moment(this.backgroundAudioManager.currentTime * 1000).format('mm:ss');
      let currentWidth = (this.backgroundAudioManager.currentTime/this.backgroundAudioManager.duration) * 450
      this.setData({
        currentTime,
        currentWidth
      })
    })

    this.backgroundAudioManager.onEnded(() => {
      //自动切换至下一首
      PubSub.publish('switchType', 'next')
      //还原实时进度条
      this.setData({
        currentWidth: 0,
        currentTime: '00:00',
      })
    });

  },

  //修改播放状态的函数
  changePlayState(isPlay){
    this.setData({
      isPlay
    })
    //修改全局变量
    appInstance.globalData.isMusicPlay = isPlay;
  },

  handleMusicPlay() {
    let isPlay = !this.data.isPlay
    // this.setData({
    //   isPlay
    // })
    let {songLocation} = this.data
    this.musicControl(isPlay,songLocation)
  },

  //控制音乐播放
  async musicControl(isPlay,songLocation){
    if (isPlay){//播放
      if (!songLocation){
        let songLocationData = await request('/song/url',{id: this.data.songId})
        let songLocation = songLocationData.data[0].url
        this.setData({
          songLocation
        })
      }
      this.backgroundAudioManager.src = this.data.songLocation;
      this.backgroundAudioManager.title = this.data.song.name;
    }else{//暂停
      this.backgroundAudioManager.pause();
    }
  },

  //点击切歌的回调
  handleSwitch(event){
    let type = event.currentTarget.id;

    this.backgroundAudioManager.stop();

    //订阅来自recommendSong页面发布的消息
    PubSub.subscribe('musicId', (msg, musicId) => {
      this.setData({
        songId: musicId
      })
      this.getSongData()
      //自动播放新的音乐
      this.musicControl(true)
      //取消订阅
      PubSub.unsubscribe('musicId');
    })

    //发布消息数据给recommendSong页面
    PubSub.publish('switchType', type)
  },

  //获取音乐详情方法
  async getSongData(){
    let songId = this.data.songId
    let song = await request('/song/detail',{ids: songId})
    let durationTime = moment(song.songs[0].dt).format('mm:ss');
    this.setData({
      song:song.songs[0],
      durationTime
    })
    //动态修改标题
    wx.setNavigationBarTitle({
      title: this.data.song.name
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})