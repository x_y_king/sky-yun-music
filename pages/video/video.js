// pages/video/video.js
import request from "../../utils/request";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    videoGroupList: [],
    navId: '',
    videoList: [],
    videoId: '',
    videoUpdateTime: [],
    isTriggered: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getVideoGroupListData();
  },

  //获取导航数据方法
  async getVideoGroupListData(){
    let videoGroupListData = await request('/video/group/list');
    this.setData({
      videoGroupList: videoGroupListData.data.slice(0,14),
      navId: videoGroupListData.data[0].id
    })
    //获取视频列表数据
    this.getVideoList(this.data.navId);
  },

  //获取视频列表数据方法
  async getVideoList(navId){
    let videoListData = await request('/video/group',{id:navId})
    wx.hideLoading();//隐藏加载
    let index = 0;
    let videoList = videoListData.datas.map(item => {
      item.id = index++;
      return item;
    })
    this.setData({
      videoList,
      isTriggered: false,//关闭下拉刷新
    })

  },

  //点击切换导航回调
  changeNav(event){
    let navId = event.currentTarget.id;
    this.setData({
      navId: navId>>>0,
      videoList:[]
    })

    //显示正在加载
    wx.showLoading({
      title: '正在加载哦',
    })

    //获取视频列表数据
    this.getVideoList(this.data.navId);
  },

  //点击播放的回调
  /*
  *多视频同时播放的问题
  *运用了js单例模式
  * */
  handlePlay(event){
    //创建video标签的实例对象
    let vid = event.currentTarget.id;
    //关闭上一个播放的视频
    //this.vid !== vid && this.videoContext && this.videoContext.stop();
    this.setData({
      videoId: vid,
    })
    //this.vid = vid;
    this.videoContext = wx.createVideoContext(vid);
    let {videoUpdateTime} = this.data;
    let videoItem = videoUpdateTime.find(item => item.vid === vid)
    if (videoItem){
      this.videoContext.seek(videoItem.currentTime);
    }
    this.videoContext.play();

  },

  //计算视频播放时间的回调
  handleTimeUpdate(event){
    let videoTimeObj = {
      vid:event.currentTarget.id,
      currentTime:event.detail.currentTime,
    }
    let {videoUpdateTime} = this.data;
    let videoItem = videoUpdateTime.find(item => item.vid === videoTimeObj.vid)
    if (videoItem){
      videoItem.currentTime = event.detail.currentTime;
    }else{
      videoUpdateTime.push(videoTimeObj);
    }
    this.setData({
      videoUpdateTime
    })
  },

  //视频播放结束回调
  handleEnded(event){
    //移除播放时长数组中当前视频的对象
    let {videoUpdateTime} = this.data;
    videoUpdateTime.splice(videoUpdateTime.findIndex(item => item.vid === event.currentTarget.id) ,1)
    this.setData({
      videoUpdateTime
    })
  },

  //自定义下拉刷新回调 scroll-view
  handleRefresher(){
    //再次发请求，获取最新的数据
    this.getVideoList(this.data.navId)
  },

  //自定义上拉触底的回调
   async handleToLower(){
    console.log("发送请求追加显示");
    let videoListData = await request('/video/group',{id:this.data.navId})
     console.log(videoListData);
    let videoList = this.data.videoList
    videoList.push(...videoListData.datas)
    this.setData({
      videoList
    })
  },

  //搜索页面跳转
  toSearch(){
    wx.navigateTo({
      url: '/pages/search/search'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})